﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Player_controller : MonoBehaviour
{
    //define variables.
    public float speed;
    Rigidbody rb;
    private int count;
    public Text countText;
    public Text winText;
    Vector3 jump;
    public float jumpForce = 2.0f;
    bool isGrounded;
    private Vector3 rotateValue;
    private int forceCount;
    private bool holding;

    // Start is called before the first frame update.
    // initialization.
    void Start()
    {
        speed = 10;
        holding = false;
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        jump = new Vector3(0,2,0);
        isGrounded = true;
    }

    
    //update the player position and add force to it.
    private void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(-moveVertical, 0.0f, moveHorizontal);
        rb.AddForce(movement*speed);
    }
    
    public void Update()
    {
        //add force to perform jump if the user press space.
        //using isGround to avoid double jump.
        if (Input.GetKeyDown("space") && isGrounded)
        {
            rb.AddForce(jump * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
        //player can rotate the ball by holding Q or E.
        if (Input.GetKey(KeyCode.Q))
        {
            rotateValue = new Vector3(0, -1, 0);
            transform.eulerAngles = transform.eulerAngles + rotateValue;
        }
        if (Input.GetKey(KeyCode.E))
        {
            rotateValue = new Vector3(0, 1, 0);
            transform.eulerAngles = transform.eulerAngles + rotateValue;
        }
        //player can hold left click to perform add force base on how long he hold.
        if (holding) 
        {
            forceCount += 10;
            Debug.Log("You're holding!");
        }
        //stop the ball if right click is performed.
        if (Input.GetKey(KeyCode.Mouse1))
        {
            rb.velocity = Vector3.zero;
        }
    }

    //the function is called if the ball collide with something.
    void OnCollisionStay()
    {
        isGrounded = true;
    }
    //the function is called if the ball is not collide.
    void OnCollisionExit()
    {
        isGrounded = false;
    }

    //run this function when the ball collide with the objects with "Pick Up" tags.
    //Also, add one to count variable for every success pick up.
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    //show the winning text when pick up all "Pick Up"
    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 10)
        {
            winText.text = "You Win!";
        }
    }

    //set holding to be true if left click is performed.
    void OnMouseDown()
    {
        holding = true;
    }

    //set holding to be true if left click is released.
    //perform the accumulated force on the ball.
    //reset the forceCount and the holding.
    private void OnMouseUp()
    {
        rb.AddForce(transform.forward * forceCount);
        forceCount = 0;
        holding = false;
        Debug.Log("You released!");
    }
}

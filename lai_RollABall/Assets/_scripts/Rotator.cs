﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{

    // Update is called once per frame
    // Everytime its called, change the rotate of transform.
    void Update()
    {
        //rotate the pick ups by time.
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }
}

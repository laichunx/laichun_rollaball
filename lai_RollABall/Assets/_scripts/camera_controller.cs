﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera_controller : MonoBehaviour
{
    public GameObject player;
    private Vector3 offset;
    private Vector3 rotateValue;


    // Start is called before the first frame update
    // get the value of offset when the game start
    void Start()
    {
        //get the offset of the camera and the ball.
        offset = transform.position - player.transform.position;
    }

    // update the camera position 
    void LateUpdate()
    {
        //set the position of the camera.
        transform.position = player.transform.position + offset;
    }
}
